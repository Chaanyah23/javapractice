public class Secondary {

    public static void main(String[] args) {

        Infiniti infiniti = new Infiniti();
        Mercedes mercedes = new Mercedes();
        BMW bmw = new BMW();
        Nissan nissan = new Nissan();

        System.out.println("Infiniti");
        System.out.println("--------------------");
        System.out.println("This car has " + infiniti.carMiles() + " miles");
        System.out.println("This car has " + infiniti.carDoors() + " doors");
        System.out.println("Gas prices for this car is " + infiniti.carGasPrice() + " premium");
        System.out.println(infiniti.carModel());
        infiniti.carDescription();

        System.out.println("\n");

        System.out.println("Mercedes Benz");
        System.out.println("--------------------");
        System.out.println("This car has " + mercedes.carMiles() + " miles");
        System.out.println("This car has " + mercedes.carDoors() + " doors");
        System.out.println("Gas for this car is " + mercedes.carGasPrice() + " premium");
        System.out.println(mercedes.carModel());
        mercedes.carDescription();

        System.out.println("\n");

        System.out.println("BMW");
        System.out.println("--------------------");
        System.out.println("This car has " + bmw.carMiles() + " miles");
        System.out.println("This car has " + bmw.carDoors() + " doors");
        System.out.println("Gas for this car is " + bmw.carGasPrice() + " premium");
        System.out.println(bmw.carModel());
        bmw.carDescription();

        System.out.println("\n");

        System.out.println("Nissan");
        System.out.println("--------------------");
        System.out.println("This car has " + nissan.carMiles() + " miles");
        System.out.println("This car has " + nissan.carDoors() + " doors");
        System.out.println("Gas for this car is " + nissan.carGasPrice() + " premium");
        System.out.println(nissan.carModel());
        nissan.carDescription();

    }

}

interface Car {

    public int carMiles();
    public int carDoors();
    public String carModel();
    public double carGasPrice();
    public void carDescription();

}

class Infiniti implements Car {

    int miles = 12145;
    int doors = 2;
    String model = "Q60s Red Sport";
    double gasPrice = 3.23;


    public int carMiles() {
        return miles;
    }

    public int carDoors() {
        return doors;
    }

    public String carModel() {
        return model;
    }

    public double carGasPrice() {
        return gasPrice;
    }

    public void carDescription() {
        System.out.println("This car is an Infiniti that has 785hp.");
    }

}

class Mercedes implements Car {

    int miles = 11452;
    int doors = 2;
    String model = "AMG GT Coupe";
    double gasPrice = 3.49;


    public int carMiles() {
        return miles;
    }

    public int carDoors() {
        return doors;
    }

    public String carModel() {
        return model;
    }

    public double carGasPrice() {
        return gasPrice;
    }

    public void carDescription() {
        System.out.println("This car is an Mercedes that has 1500hp.");
    }

}

class BMW implements Car {

    int miles = 10320;
    int doors = 4;
    String model = "M5 CS";
    double gasPrice = 3.43;


    public int carMiles() {
        return miles;
    }

    public int carDoors() {
        return doors;
    }

    public String carModel() {
        return model;
    }

    public double carGasPrice() {
        return gasPrice;
    }

    public void carDescription() {
        System.out.println("This car is an BMW that has 1200hp.");
    }

}

class Nissan implements Car {

    int miles = 5632;
    int doors = 2;
    String model = "GTR";
    double gasPrice = 3.03;


    public int carMiles() {
        return miles;
    }

    public int carDoors() {
        return doors;
    }

    public String carModel() {
        return model;
    }

    public double carGasPrice() {
        return gasPrice;
    }

    public void carDescription() {
        System.out.println("This car is an Nissan that has 1485hp.");
    }

}