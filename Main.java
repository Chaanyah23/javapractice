public class Main {

    public static void main(String[] args) {

        ChromeBrowser chrome = new ChromeBrowser();
        System.out.println(chrome.getBrowserName());
        chrome.openPageURL();

        System.out.println("\n");

        IEBrowser ie = new IEBrowser();
        System.out.println(ie.getBrowserName());
        ie.openPageURL();

    }

}

abstract class WebBrowser {

    abstract public String getBrowserName();
    abstract public void openPageURL();

}

class ChromeBrowser extends WebBrowser {

    public String browserName = "Chrome";

    public String getBrowserName() {
        return browserName;
    }

    public void openPageURL() {
        System.out.println("www.google.com");
    }

}

class IEBrowser extends WebBrowser {

    public String browserName = "Internet Explorer";

    public String getBrowserName() {
        return browserName;
    }

    public void openPageURL() {
        System.out.println("www.microsoft.com");
    }

}


